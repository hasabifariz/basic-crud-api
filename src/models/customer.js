const customerModel = `
    type Customer {
        id: ID
        firstName: String
        lastName: String
        bornIn: String
        bornYear: Int
    }
`;

export default customerModel