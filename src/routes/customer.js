require("dotenv").config();
import customerModel from "../models/customer";
import { nanoid } from "nanoid";

const express = require("express");
const { OGM } = require("@neo4j/graphql-ogm");
const neo4j = require("neo4j-driver");

const { URI, DB_USERNAME, DB_PASSWORD, DB_NAME } = process.env;

const driver = neo4j.driver(URI, neo4j.auth.basic(DB_USERNAME, DB_PASSWORD));

const typeDefs = customerModel
const ogm = new OGM({ typeDefs, driver });
const Customer = ogm.model("Customer");

const router = express();

router.get("/", async (req, res) => {
  const customers = await Customer.find();
  return res.json(customers).end();
});

router.get("/:id", async (req, res) => {
  const customers = await Customer.find({ where: { id: req.params.id } });
  if (!customers.length) {
    return res
      .status(400)
      .json({
        status: 400,
        data: customers,
      })
      .end();
  }
  return res
    .status(200)
    .json({
      status: 200,
      data: customers,
    })
    .end();
});
router.post("/", async (req, res) => {
  const request = req.body;
  const customers = await Customer.create({
    input: [
      {
        id: nanoid(8),
        firstName: request.firstName,
        lastName: request.lastName,
        bornIn: request.bornIn,
        bornYear: request.bornYear,
      },
    ],
  });

  return res.json(customers).end();
});
router.put("/:id", async (req, res) => {
  const request = req.body;
  const customers = await Customer.update({
    where: { id: req.params.id },
    update: {
      firstName: request.firstName,
      lastName: request.lastName,
      bornIn: request.bornIn,
      bornYear: request.bornYear,
    },
  });
  if (!customers.length) {
    return res
      .status(400)
      .json({
        status: 400,
        data: customers,
      })
      .end();
  }
  return res
    .status(200)
    .json({
      status: 200,
      data: customers,
    })
    .end();
});
router.delete("/:id", async (req, res) => {
  const customers = await Customer.delete({ where: { id: req.params.id } });
  if (customers.nodesDeleted === 0 && customers.relationshipsDeleted === 0) {
    return res
      .status(400)
      .json({
        status: 400,
        data: customers,
      })
      .end();
  }
  return res
    .status(200)
    .json({
      status: 200,
      data: customers,
    })
    .end();
});

export default router;
