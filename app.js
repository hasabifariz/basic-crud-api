require("dotenv").config();
var logger = require("morgan");
import express from "express";
import customer from "./src/routes/customer";

const app = express();
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/customer", customer);

app.listen(process.env.LISTEN_PORT);
console.log('Listening on port:',process.env.LISTEN_PORT )
